*** WooCommerce Subscription Downloads ***

2017-08-03 - version 1.1.9
 * Fix - When product is in draft mode, subscribers can see the downloadable product in My Account.
 * Fix - Cancelled or Expired subscriptions still grant access to downloadable files.

2017-06-28 - version 1.1.8
 * Fix - PHP notice when attaching subscriptions to downloadable products.

2017-05-31 - version 1.1.7
 * Fix - Cannot attach a Subscription to a variation

2017-04-25 - version 1.1.6
 * Fix - Downloads will now attach to Subscriptions instead of Orders
 * Fix - Handle subscription status changes with respect to download availability.
 * Fix - Issue with searching subscriptions returning results that don't match the criteria

2017-04-03 - version 1.1.5
 * Fix - Update for WooCommerce 3.0 compatibility

2016-01-06 - version 1.1.4
 * Fix - Deprecated function call to Subscription 2.0's function

2015-12-09 - version 1.1.3
 * Fix - Unable to choose specific variation

2015-11-07 - version 1.1.2
 * Fixed - Save subscriptions information when duplicating product.

2015-06-30 - version 1.1.1
 * Fixed - Plugin initialization.

2015-02-12 - version 1.1.0
 * New - Support to add downloads to specific variations of a variable subscription
 * Fix - Display of the "Subscriptions" field in variation product panel
 * Fix - Download link when the customer purchase a variable subscription

2015-01-29 - version 1.0.3
 * Fix - Backwards compatibility

2015-01-28 - version 1.0.2
 * Fix - WC 2.3 compatibility

2015-01-27 - version 1.0.1
 * New - WC 2.3 compatibility (chosen -> select2).

2014-06-03 - version 1.0.0
 * First Release.
