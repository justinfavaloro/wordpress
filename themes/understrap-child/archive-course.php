<?php
/**
 * The Template for displaying course archives, including the course page template.
 *
 * Override this template by copying it to your_theme/sensei/archive-course.php
 *
 * @author 		Automattic
 * @package 	Sensei
 * @category    Templates
 * @version     1.9.0
 */

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="archive-wrapper">

    <div class="<?php echo esc_html( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part( 'global-templates/left-sidebar-check', 'none' ); ?>

            <main class="site-main" id="main">

            <?php

                    /**
                     * This action before course archive loop. This hook fires within the archive-course.php
                     * It fires even if the current archive has no posts.
                     *
                     * @since 1.9.0
                     *
                     * @hooked Sensei_Course::course_archive_sorting 20
                     * @hooked Sensei_Course::course_archive_filters 20
                     * @hooked Sensei_Templates::deprecated_archive_hook 80
                     */
                    do_action( 'sensei_archive_before_course_loop' );

                ?>

                <?php if ( have_posts() ): ?>

                    <?php sensei_load_template( 'loop-course.php' ); ?>

                <?php else: ?>

                    <p><?php _e( 'No courses found that match your selection.', 'woothemes-sensei' ); ?></p>

                <?php  endif; // End If Statement ?>

             

                <?php

                    /**
                     * This action runs after including the course archive loop. This hook fires within the archive-course.php
                     * It fires even if the current archive has no posts.
                     *
                     * @since 1.9.0
                     */
                    do_action( 'sensei_archive_after_course_loop' );

                ?>

            </main><!-- #main -->

            <!-- The pagination component -->
            <?php understrap_pagination(); ?>

        </div><!-- #primary -->

        <!-- Do the right sidebar check -->
        <?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

            <?php get_sidebar( 'right' ); ?>

        <?php endif; ?>

    </div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>


    

       <?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>
            </div>
            <?php get_sidebar( 'right' ); ?>

        <?php endif; ?>


<?php get_sensei_footer(); ?>