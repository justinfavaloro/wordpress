<?php

require 'shortcodes/class-blog-posts-shortcode.php';
require 'shortcodes/class-shortcode-loader.php';

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}


add_action('sensei_before_main_content', 'my_theme_wrapper_start', 10);
add_action('sensei_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  ?><div class="wrapper" id="page-wrapper">
        <div class="container" id="content" tabindex="-1">
        <div class="row">
        <!-- Do the left sidebar check -->
        <div class="content-area <?php echo (is_active_sidebar('right-sidebar') ? 'col-md-8' : 'col-md-12'); ?>" id="primary">
        <div class="site-main" id="main"><?php
}

function my_theme_wrapper_end() {
  echo '</div><!-- #content -->
        </div><!-- #content -->
        </div><!-- #content -->
	</div><!-- #container -->
    </div><!-- #container -->';
	get_sidebar();
}

add_filter( 'sensei_course_loop_number_of_columns', 'reset_columns' );

function reset_columns($items) {
    return 3;
}

function course_edit_content() {
    remove_action('sensei_archive_before_course_loop', array( 'Sensei_Course', 'course_archive_sorting' ));
    remove_action('sensei_archive_before_course_loop', array( 'Sensei_Course', 'course_archive_filters' ));
    remove_action( 'sensei_course_content_inside_before', array('Sensei_Templates', 'the_title'), 5);
    add_action( 'sensei_course_content_inside_before', 'display_title', 15);
    remove_action( 'sensei_course_content_inside_before', array(Sensei()->course, 'the_course_meta'), 10);
    add_action( 'sensei_course_content_inside_after', 'the_course_meta');
    remove_action( 'sensei_course_content_inside_before', array(Sensei()->course, 'course_image'), 10);
    // remove_action( 'sensei_single_course_content_inside_before', array( Sensei()->course , 'course_image'), 20 );
    // add_action( 'sensei_single_course_content_inside_before', array( Sensei()->course , 'course_image'), 5 );
    
}

function display_title() {
    the_title('<h3 class="card-title">', '</h3>' );
}

add_action( 'init', 'course_edit_content' );


global $woothemes_sensei;
remove_action( 'sensei_before_main_content', array( $woothemes_sensei->frontend, 'sensei_output_content_wrapper' ), 10 );
remove_action( 'sensei_after_main_content', array( $woothemes_sensei->frontend, 'sensei_output_content_wrapper_end' ), 10 );


add_action( 'after_setup_theme', 'declare_sensei_support' );
function declare_sensei_support() {
    add_theme_support( 'sensei' );
}

function wpb_custom_billing_fields( $fields = array() ) {
    unset($fields['billing_company']);
    unset($fields['billing_address_1']);
    unset($fields['billing_address_2']);
    unset($fields['billing_state']);
    unset($fields['billing_city']);
    unset($fields['billing_phone']);
    unset($fields['billing_postcode']);
    unset($fields['billing_country']);
    return $fields;
}


add_filter('woocommerce_billing_fields','wpb_custom_billing_fields');

function custom_override_checkout_fields_ek( $fields ) {
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_state']);
    return $fields;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields_ek', 99 );

function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    unset($items['edit-address']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );

add_filter( 'post_class', 'remove_hentry_function', 20 );
function remove_hentry_function( $classes ) {
    if(!is_singular()) {
        $classes[] = 'col-sm-6';
        $classes[] = 'col-xs-12';  
    }
    return $classes;
}

function add_column_class( $classes ) {
    $classes[] = 'col-md-4';
    return $classes;
}

//add_action('understrap_after_nav_content', 'add_slider', 15);

function add_slider() { 
    if(is_front_page()) {
        ?><div class="jumbotron">
          <div class="container">
            <h1 class="display-4">Online Coaching</h1>
            <p>Have you recently come off from an injury and afraid of reinjurying yourself? Or you're not performing as well as you would like in your sport and not sure how to improve? At Athletic Performance we are here to help guide you along the way to reaching your goals. We offer programs for a variety of goals, as well as personalised programs for more specific requirements. For the first 10 members you will receive the first month of online coaching for free.</p>
            <p><a class="btn btn-primary" href="/coaching" role="button">Find out more</a></p>
          </div>
        </div><?php
    }
    
}

function add_course_content() {
    //echo do_shortcode('[sensei_featured_courses number="3"]'); 
    echo do_shortcode('[blog_posts number="3"]'); 
}

add_action('homepage', 'add_course_content', 5);


add_action( 'wp', 'check_home' );

function check_home() {
    if(is_front_page()) {
        add_action('sensei_loop_course_before', 'add_course_header');
        add_action('sensei_loop_course_after', 'add_view_all_courses_link');
        add_action('loop_post_before', 'add_post_header');
        add_action('loop_post_after', 'add_view_all_posts_link');    
        
    }
    else {
         add_action('loop_post_before', 'add_blog_header'); 
    }

    if(is_archive() || is_front_page()) {
        add_action( 'sensei_course_content_inside_after', 'add_progress_to_course' );
    }

    if((!is_active_sidebar( 'right-sidebar' ) && !is_singular()) || is_front_page())  {
            add_filter( 'post_class', 'add_column_class', 5 );
    }
}

function add_progress_to_course($course_id) {
    global $post;
    if(Sensei_Utils::user_started_course($post->ID, get_current_user_id())) {
        Sensei_Shortcode_User_Courses::attach_course_progress($course_id);
    }
}

function add_post_header() {
    ?><h2 class="title">Latest Posts</h1><?php
}

function add_blog_header() {
    ?><header class="entry-header">
      <h1 class="entry-title">Blog</h1> 
    </header><?php
}

function add_course_header() {
    ?><h2 class="title">Featured Courses</h1><?php
}

function add_view_all_courses_link() {
    ?><div class="row justify-content-md-center">
        <div class="col-12 col-md-auto">
            <a class="btn btn-primary" href="/courses">View more courses</a>
        </div> 
    </div><?php
}

function add_view_all_posts_link() {
    ?><div class="row justify-content-md-center">
        <div class="col-12 col-md-auto">
            <a class="btn btn-primary" href="/blog">View more articles</a>
        </div> 
    </div><?php
}


function override_sensei_javascript() { ?>

        <script type="text/javascript">
            var courseList = jQuery('ul.course-container');

            jQuery( 'a#sensei-user-courses-active-action').trigger( 'click' );
            ///
            /// FUNCTIONS
            ///
            function sensei_user_courses_hide_all_completed(){

                jQuery('article.user-completed').hide();

            }

            function sensei_user_courses_hide_all_active(){

                jQuery('article.user-active').hide();

            }

            function sensei_user_courses_show_all_completed(){
                console.log(jQuery('article.user-completed'));
                jQuery('article.user-completed').show();

            }

            function sensei_user_courses_show_all_active(){

                jQuery('article.user-active').show();

            }


        </script>

    <?php }

    add_action( 'wp_footer', 'override_sensei_javascript',950 );

    function understrap_posted_on() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
    if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
        $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
    }
    $time_string = sprintf( $time_string,
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() ),
        esc_attr( get_the_modified_date( 'c' ) ),
        esc_html( get_the_modified_date() )
    );
    
    $byline = sprintf(
        esc_html_x( 'by %s', 'post author', 'understrap' ),
        '<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
    );
    if(is_single()) {
        echo '<span class="posted-on">' .  $time_string . ' </span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
    }
    else {
        echo '<span class="posted-on pull-right">' .  $time_string . '</span><span class="byline pull-left"> ' . $byline . '</span>'; // WPCS: XSS OK.    
    }
    
}

function all_excerpts_get_more_link( $post_excerpt ) {
    return $post_excerpt;
}

add_filter('sensei_wc_single_add_to_cart_button_text', 'course_add_to_cart_text'); 

function course_add_to_cart_text($button_text) {
    $_pf = new WC_Product_Factory(); 
    $product_id = Sensei_WC::get_course_product_id(get_the_ID());
    $product = $_pf->get_product($product_id);
    $button_text = 'Enroll in Course for '.$product->get_price_html();
    return $button_text;
}

function display_modules( $atts ) {
    ob_start();
    get_template_part('single-course/modules');
    $output = ob_get_contents();
ob_end_clean();
return $output;
}
add_shortcode( 'modules', 'display_modules' );

function the_course_meta( $course_id ){
        echo '<p class="sensei-course-meta">';

        $course = get_post( $course_id );
        $author_display_name = get_the_author_meta( 'display_name', $course->post_author  );

        if ( isset( Sensei()->settings->settings[ 'course_author' ] ) && ( Sensei()->settings->settings[ 'course_author' ] ) ) {?>

            <span class="pull-left course-author"><?php _e( 'by ', 'woothemes-sensei' ); ?>

                <?php echo esc_attr( $author_display_name ); ?>

            </span>

        <?php }  // End If Statement ?>
        <span class="pull-right <?php echo (Sensei_Utils::user_started_course($course_id, get_current_user_id()) ? '' : 'hidden'); ?>">
         <?php 
         $percentage = Sensei()->course->get_completion_percentage( $course_id, get_current_user_id());
         echo $percentage. '% Complete';
         ?>

                         
        </span>

        <span class="pull-right <?php echo (!Sensei_Utils::user_started_course($course_id, get_current_user_id()) ? '' : 'hidden'); ?>">
        <?php sensei_simple_course_price( $course->ID ); ?>   
        </span>
        <?php

        echo '</p>';
    } // end the course meta

 add_filter('woocommerce_get_price_html', 'changeFreePriceNotice', 10, 2);
 
function changeFreePriceNotice($price, $product) {
    if ( $price == wc_price( 0.00 ) )
        return 'FREE';
    else
        return $price;
}

add_filter('comment_class', 'add_class_to_comment_item'); 

function add_class_to_comment_item() {
    $classes[] = 'list-group-item';
    return $classes;
}


add_action('pre_get_posts', 'remove_client_programs' );

function remove_client_programs( $wp_query ) {
    if( is_archive('course') && !is_admin()) {
        $excluded = array( 
            array(
                'taxonomy' => 'course-category',
                'terms' => 'clients',
                'field' => 'slug',
                'operator' => 'NOT IN',
            )
        );
        set_query_var('tax_query', $excluded);
    }
}

add_action('woocommerce_payment_complete', 'create_course_on_complete_order', 10, 1);

function create_course_on_complete_order($order_id) {
    $order = new WC_Order( $order_id );
    $items = $order->get_items();
    $gold_membership_id = 172;
    foreach ($items as $item) {
        if ($item['product_id']== $gold_membership_id ) {
            create_individual_course_for_gold_membership((int)$order->user_id);
        }
    }
    return $order_id;
}

add_action('wc_memberships_user_membership_saved', 'create_course_on_membership_added', 10);

function create_course_on_membership_added($membership_plan, $args = array() ) {

    $gold_membership_id = 172;
    if($membership_plan->id == $gold_membership_id) {
        create_individual_course_for_gold_membership(get_current_user_id());
    }
}


function create_individual_course_for_gold_membership($user_id)
{
    $user_info = get_userdata($user_id);
    $title = $user_info->display_name."'s Program";
    if(get_page_by_title($title, OBJECT, 'course') == NULL) {
        remove_action( 'save_post', 'save_book_meta', 10, 3 );
        $post_id = wp_insert_post(array(
           'post_type' => 'course',
           'post_title' => $title,
           'post_content' => 'test content',
           'post_status' => 'publish',
        ));
        wp_set_post_terms( $post_id, '20', 'course-category');
        Sensei_Utils::user_start_course($user_id, $post_id); 
    }
}


function save_book_meta( $post_id, $post, $update ) {

    /*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $post_type = get_post_type($post_id);

    // If this isn't a 'book' post, don't update it.
    if ( "course" != $post_type ) return;

    $thumbnail_id = get_post_thumbnail_id( $post_id );
    // - Update the post's metadata.
    $product_id = Sensei_WC::get_course_product_id($post_id);
    if ($product_id == NULL) {

        if(!has_term( 'clients', 'course-category' )) {
            $product_id = wp_insert_post(array(
               'post_type' => 'product',
               'post_title' => $post->post_title,
               'post_content' => $post->post_content,
               'post_excerpt' => $post->post_excerpt,
               'post_status' => 'publish',
            ));
            update_post_meta($post_id, '_course_woocommerce_product', $product_id, null );
            set_post_thumbnail( $product_id, $thumbnail_id );
            wp_set_post_terms( $product_id, '28', 'product_cat');
        }
    }
    else {
        $product = array(
            'ID' => $product_id,
            'post_title' => $post->post_title,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt
        );
        wp_update_post( $product );
    }
    

}
add_action( 'save_post', 'save_book_meta', 10, 3 );


add_action('woocommerce_order_details_after_order_table', 'display_courses');

function display_courses($order) {
    $courses = array();
    foreach ( $order->get_items() as $item_id => $item ) {
        $course = get_page_by_title($item->get_name(), OBJECT, 'course');
        if($course != null) {
            $courses[] = $course;
        }
    }
     
    if(count($courses) > 0) { ?>
        <ul class="list-group">
            <li class="list-group-item"><b>Courses</b></li>
            <?php
                foreach ( $order->get_items() as $item_id => $item ) {
                    $product = get_page_by_title($item->get_name(), OBJECT, 'course'); ?>
                    <li class="list-group-item"><a href="<?php echo get_permalink($product->ID); ?>"><?php echo $product->post_title; ?></a></li>
                <?php }
            ?>
            </ul>
    <?php }                         

}
