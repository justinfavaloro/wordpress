<?php
/**
 * This runs before the the post loop items in the loop.php template. It runs
 * only only for the post post type. This loop will not run if the current wp_query
 * has no posts.
 *
 * @since 1.9.0
 */
do_action( 'loop_post_before' );
?>

<div class="row post-container" >

    <?php
    /**
     * This runs before the post type items in the loop.php template. It
     * runs within the courses loop <ul> tag.
     *
     * @since 1.9.0
     */
    do_action( 'loop_post_inside_before' );
    ?>

    <?php
    /*
     * Loop through all posts
     */
    while ( have_posts() ) { the_post();

         get_template_part( 'loop-templates/content', 'post' );

    }
    ?>

    <?php
    /** 
     * This runs after the post type items in the loop.php template. It runs
     * only for the specified post type
     *
     * @since 1.9.0
     */
    do_action( 'loop_post_inside_after' );
    ?>

</div>

<?php
/**
 * This runs after the post type items in the loop.php template. It runs
 * only for the specified post type
 *
 * @since 1.9.0
 */
do_action( 'loop_post_after' );
?>