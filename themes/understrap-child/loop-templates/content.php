<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<a href="<?php echo get_permalink(); ?>">
		<div class="card">
			<?php echo get_the_post_thumbnail( $post->ID, 'thumbnail', array('class' => 'card-img-top') ); ?>
			<div class="card-block">
				<header class="entry-header">

					<?php the_title('<h3 class="card-title">', '</h3>' ); ?>				

				</header><!-- .entry-header -->

				<div class="card-text">

					<?php
					the_excerpt();
					?>

					<?php if ( 'post' == get_post_type() ) : ?>

						<p class="entry-meta">
							<?php understrap_posted_on(); ?>
						</p><!-- .entry-meta -->

					<?php endif; ?>

					<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
						'after'  => '</div>',
					) );
					?>

				</div><!-- .entry-content -->
			</div>
		</div>
	</a>
</article><!-- #post-## -->
