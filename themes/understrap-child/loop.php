<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The Template for outputting Lists of any Sensei content type.
 *
 * This template expects the global wp_query to setup and ready for the loop
 *
 * @author 		Automattic
 * @package 	Sensei
 * @category    Templates
 * @version     1.9.0
 */
?>

<div class="row" >

    <?php
    /*
     * Loop through all courses
     */
    while ( have_posts() ) { the_post();

        get_template_part( 'loop-templates/content', get_post_format() );

    }
    ?>


</div>
