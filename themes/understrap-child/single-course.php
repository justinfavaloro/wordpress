<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="single-wrapper">

    <div class="<?php echo esc_html( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part( 'global-templates/left-sidebar-check', 'none' ); ?>

            <main class="site-main" id="main">

                <article <?php post_class( array( 'course', 'post' ) ); ?>>

                    <?php

                    /**
                     * Hook inside the single course post above the content
                     *
                     * @since 1.9.0
                     *
                     * @param integer $course_id
                     *
                     * @hooked Sensei()->frontend->sensei_course_start     -  10
                     * @hooked Sensei_Course::the_title                    -  10
                     * @hooked Sensei()->course->course_image              -  20
                     * @hooked Sensei_WC::course_in_cart_message           -  20
                     * @hooked Sensei_Course::the_course_enrolment_actions -  30
                     * @hooked Sensei()->message->send_message_link        -  35
                     * @hooked Sensei_Course::the_course_video             -  40
                     */
                    do_action( 'sensei_single_course_content_inside_before', get_the_ID() );

                    ?>

                    <section class="entry fix">

                        <?php the_content(); ?>

                    </section>

                    <?php

                    /**
                     * Hook inside the single course post above the content
                     *
                     * @since 1.9.0
                     *
                     * @param integer $course_id
                     *
                     */
                    do_action( 'sensei_single_course_content_inside_after', get_the_ID() );

                    ?>
                </article><!-- .post .single-course -->

            </main><!-- #main -->

        </div><!-- #primary -->

        <!-- Do the right sidebar check -->
        <?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

            <?php get_sidebar( 'right' ); ?>

        <?php endif; ?>

    </div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->
<?php get_footer(); ?>